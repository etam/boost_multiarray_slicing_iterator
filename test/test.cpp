//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

#include <boost/multi_array.hpp>

#include <etam/slicing_iterator.hpp>


class Image
{
public:
    typedef boost::multi_array<int, 2> DataType;

private:
    DataType data;

    Image(DataType data_)
        : data{std::move(data_)}
    {}

public:
    static
    Image fromVector(const std::vector<int>& data_, const std::size_t height, const std::size_t width)
    {
        auto data = DataType(boost::extents[height][width]);
        for (std::size_t y = 0; y < height; ++y) {
            for (std::size_t x = 0; x < width; ++x) {
                data(std::array<std::size_t,2>{{y,x}}) = data_[y*width + x];
            }
        }
        return {std::move(data)};
    }

    std::size_t height() const
    {
        return data.shape()[0];
    }

    std::size_t width() const
    {
        return data.shape()[1];
    }

    auto rows_begin()
    {
        return etam::makeSlicingIterator<0>(data, 0);
    }
    auto rows_begin() const
    {
        return etam::makeConstSlicingIterator<0>(data, 0);
    }
    auto rows_cbegin() const
    {
        return rows_begin();
    }

    auto rows_end()
    {
        return etam::makeSlicingIterator<0>(data, height());
    }
    auto rows_end() const
    {
        return etam::makeConstSlicingIterator<0>(data, height());
    }
    auto rows_cend() const
    {
        return rows_end();
    }

    auto columns_begin()
    {
        return etam::makeSlicingIterator<1>(data, 0);
    }
    auto columns_begin() const
    {
        return etam::makeConstSlicingIterator<1>(data, 0);
    }
    auto columns_cbegin() const
    {
        return columns_begin();
    }

    auto columns_end()
    {
        return etam::makeSlicingIterator<1>(data, width());
    }
    auto columns_end() const
    {
        return etam::makeConstSlicingIterator<1>(data, width());
    }
    auto columns_cend() const
    {
        return columns_end();
    }
};


template <typename It>
void assignImageValues(It lineFirst, It lineLast)
{
    int i = 0;
    std::for_each(lineFirst, lineLast, [&](auto lineView) mutable {
        for (int& pixel : lineView) {
            pixel = i++;
        }
    });
}

void assignByRows(Image& image)
{
    assignImageValues(image.rows_begin(), image.rows_end());
}

void assignByColumns(Image& image)
{
    assignImageValues(image.columns_begin(), image.columns_end());
}


template <typename It>
void printImage(It lineFirst, It lineLast, std::ostream& ostream)
{
    std::for_each(lineFirst, lineLast, [&](auto lineView) mutable {
        for (const int pixel : lineView) {
            ostream << pixel << ",";
        }
        ostream << '\n';
    });
    ostream << '\n';
}

void printByRows(const Image& image, std::ostream& ostream)
{
    printImage(image.rows_begin(), image.rows_end(), ostream);
}

void printByColumns(const Image& image, std::ostream& ostream)
{
    printImage(image.columns_begin(), image.columns_end(), ostream);
}


int main()
{
    auto image = Image::fromVector({0,0,0,0, 0,0,0,0, 0,0,0,0}, 3, 4);
    assignByColumns(image);
    printByRows(image, std::cout);
    assignByRows(image);
    printByColumns(image, std::cout);
}
