//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_SLICING_ITERATOR_SLICE_INDICES_HPP
#define ETAM_SLICING_ITERATOR_SLICE_INDICES_HPP

#include <type_traits>

#include <boost/multi_array.hpp>

#include "indices.hpp"


namespace etam {


namespace detail {

template <std::size_t dimensions, std::size_t sliceDimension, typename Enable = void>
struct SliceIndices;

template <std::size_t dimensions, std::size_t sliceDimension>
struct SliceIndices<dimensions, sliceDimension, std::enable_if_t<(sliceDimension < dimensions-1)>>
{
    static
    auto get(const std::size_t sliceIdx)
    {
        typedef boost::multi_array_types::index_range range;
        return SliceIndices<dimensions-1, sliceDimension>::get(sliceIdx)[range()];
    }
};

template <std::size_t dimensions, std::size_t sliceDimension>
struct SliceIndices<dimensions, sliceDimension, std::enable_if_t<(sliceDimension == dimensions-1)>>
{
    static
    auto get(const std::size_t sliceIdx)
    {
        return Indices<dimensions-1>::get()[sliceIdx];
    }
};

} // namespace detail


template <std::size_t dimensions, std::size_t sliceDimension>
auto sliceIndices(const std::size_t sliceIdx)
{
    return detail::SliceIndices<dimensions, sliceDimension>::get(sliceIdx);
}


} // namespace etam

#endif // ETAM_SLICING_ITERATOR_SLICE_INDICES_HPP