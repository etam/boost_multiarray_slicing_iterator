//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_SLICING_ITERATOR_CONST_SLICING_ITERATOR_HPP
#define ETAM_SLICING_ITERATOR_CONST_SLICING_ITERATOR_HPP

#include <boost/iterator/iterator_facade.hpp>
#include <boost/multi_array.hpp>

#include "slice_indices.hpp"


namespace etam {


namespace detail {

template <typename T, std::size_t dimensions, std::size_t sliceDimension>
class ConstSlicingIterator
    : public boost::iterator_facade<
        ConstSlicingIterator<T, dimensions, sliceDimension>,
        boost::detail::multi_array::const_multi_array_view<T, dimensions-1>,
        boost::random_access_traversal_tag,
        boost::detail::multi_array::const_multi_array_view<T, dimensions-1>
      >
{
    static_assert(dimensions > 1, "dimensions < 2");
    static_assert(sliceDimension < dimensions, "sliceDimension >= dimensions");
public:
    typedef boost::detail::multi_array::const_multi_array_view<T, dimensions> SourceViewType;

private:
    SourceViewType view;
    std::size_t idx;

public:
    ConstSlicingIterator() = default;

    ConstSlicingIterator(SourceViewType view_, std::size_t idx_)
        : view{std::move(view_)}
        , idx{idx_}
    {}

    auto dereference() const
    {
        return view[sliceIndices<dimensions, sliceDimension>(idx)];
    }

    bool equal(const ConstSlicingIterator& other) const
    {
        return view.origin() == other.view.origin()
            && idx == other.idx;
    }

    void increment()
    {
        ++idx;
    }

    void decrement()
    {
        --idx;
    }

    void advance(std::size_t n)
    {
        idx += n;
    }

    auto distance_to(const ConstSlicingIterator& other) const
    {
        return other.idx - idx;
    }
};

template <typename T, std::size_t dimensions, std::size_t sliceDimension>
struct ConstSlicingIteratorType
{
    typedef ConstSlicingIterator<T, dimensions, sliceDimension> type;
};

template <typename T>
struct ConstSlicingIteratorType<T, 1, 0>
{
    typedef typename boost::multi_array<T,1>::const_iterator type;
};

} // namespace detail


template <typename T, std::size_t dimensions, std::size_t sliceDimension>
using ConstSlicingIterator = typename detail::ConstSlicingIteratorType<T, dimensions, sliceDimension>::type;


template <std::size_t sliceDimension, typename T, std::size_t dimensions>
auto makeSlicingIterator(const boost::multi_array<T, dimensions>& multiArray, std::size_t sliceIdx)
{
    static_assert(dimensions > 1, "dimensions < 2");
    static_assert(sliceDimension < dimensions, "sliceDimension >= dimensions");
    return ConstSlicingIterator<T, dimensions, sliceDimension>(multiArray[indices<dimensions>()], sliceIdx);
}

template <std::size_t sliceDimension, typename T, std::size_t dimensions>
auto makeConstSlicingIterator(const boost::multi_array<T, dimensions>& multiArray, std::size_t sliceIdx)
{
    return makeSlicingIterator<sliceDimension>(multiArray, sliceIdx);
}


} // namespace etam

#endif //  ETAM_SLICING_ITERATOR_CONST_SLICING_ITERATOR_HPP
