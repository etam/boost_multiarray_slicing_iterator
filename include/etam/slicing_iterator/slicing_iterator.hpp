//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_SLICING_ITERATOR_SLICING_ITERATOR_HPP
#define ETAM_SLICING_ITERATOR_SLICING_ITERATOR_HPP

#include <boost/iterator/iterator_facade.hpp>
#include <boost/multi_array.hpp>

#include "slice_indices.hpp"


namespace etam {


namespace detail {

template <typename T, std::size_t dimensions, std::size_t sliceDimension>
class SlicingIterator
    : public boost::iterator_facade<
        SlicingIterator<T, dimensions, sliceDimension>,
        boost::detail::multi_array::multi_array_view<T, dimensions-1>,
        boost::random_access_traversal_tag,
        boost::detail::multi_array::multi_array_view<T, dimensions-1>
      >
{
    static_assert(dimensions > 1, "dimensions < 2");
    static_assert(sliceDimension < dimensions, "sliceDimension >= dimensions");
public:
    typedef boost::detail::multi_array::multi_array_view<T, dimensions> SourceViewType;

private:
    SourceViewType view;
    std::size_t idx;

public:
    SlicingIterator() = default;

    SlicingIterator(SourceViewType view_, std::size_t idx_)
        : view{std::move(view_)}
        , idx{idx_}
    {}

    auto dereference() const
    {
        return const_cast<SlicingIterator*>(this)->view[sliceIndices<dimensions, sliceDimension>(idx)];
    }

    bool equal(const SlicingIterator& other) const
    {
        return view.origin() == other.view.origin()
            && idx == other.idx;
    }

    void increment()
    {
        ++idx;
    }

    void decrement()
    {
        --idx;
    }

    void advance(std::size_t n)
    {
        idx += n;
    }

    auto distance_to(const SlicingIterator& other) const
    {
        return other.idx - idx;
    }
};

template <typename T, std::size_t dimensions, std::size_t sliceDimension>
struct SlicingIteratorType
{
    typedef SlicingIterator<T, dimensions, sliceDimension> type;
};

template <typename T>
struct SlicingIteratorType<T, 1, 0>
{
    typedef typename boost::multi_array<T,1>::iterator type;
};

} // namespace detail


template <typename T, std::size_t dimensions, std::size_t sliceDimension>
using SlicingIterator = typename detail::SlicingIteratorType<T, dimensions, sliceDimension>::type;


template <std::size_t sliceDimension, typename T, std::size_t dimensions>
auto makeSlicingIterator(boost::multi_array<T, dimensions>& multiArray, std::size_t sliceIdx)
{
    static_assert(dimensions > 1, "dimensions < 2");
    static_assert(sliceDimension < dimensions, "sliceDimension >= dimensions");
    return SlicingIterator<T, dimensions, sliceDimension>(multiArray[indices<dimensions>()], sliceIdx);
}


} // namespace etam

#endif //  ETAM_SLICING_ITERATOR_SLICING_ITERATOR_HPP
