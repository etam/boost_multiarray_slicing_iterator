//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_SLICING_ITERATOR_INDICES_HPP
#define ETAM_SLICING_ITERATOR_INDICES_HPP

#include <boost/multi_array.hpp>


namespace etam {


namespace detail {

template <std::size_t dimensions>
struct Indices
{
    static
    auto get()
    {
        typedef boost::multi_array_types::index_range range;
        return Indices<dimensions-1>::get()[range()];
    }
};

template <>
struct Indices<0>
{
    static
    auto get()
    {
        return boost::indices;
    }
};

} // namespace detail


template <std::size_t dimensions>
auto indices()
{
    return detail::Indices<dimensions>::get();
}


} // namespace etam

#endif // ETAM_SLICING_ITERATOR_INDICES_HPP
