//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2015.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_SLICING_ITERATOR_HPP
#define ETAM_SLICING_ITERATOR_HPP

#include "slicing_iterator/const_slicing_iterator.hpp"
#include "slicing_iterator/slicing_iterator.hpp"

#endif //  ETAM_SLICING_ITERATOR_HPP
